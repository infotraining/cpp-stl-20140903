#include <iostream>
#include <unordered_set>
#include <functional>
#include <boost/functional/hash.hpp>

using namespace std;

template <typename Container>
void print(const Container& container, const string& prefix = "container")
{
    cout << prefix << ": [ ";

    for(const auto& item : container)
        cout << item << " ";

    cout << "]" << endl;
}

template <typename Container>
void show_stat(const Container& cont, const string& prefix)
{
    cout << prefix << " - "
         << "Bucket count: " << cont.bucket_count()
         << "; Load factor: " << cont.load_factor()
         << "; Max load factor: " << cont.max_load_factor() << endl;
}

class Person
{
    int id_;
    string name_;

public:
    Person(int id, const string& name) : id_{id}, name_{name}
    {
    }

    bool operator<(const Person& other) const
    {
        return id_ < other.id_;
    }

    bool operator==(const Person& other) const
    {
        if (id_ == other.id_)
            return name_ == other.name_;
        return false;
    }

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }
};

namespace std
{
    template <>
    class hash<Person>
    {
    public:
        size_t operator()(const Person& p) const
        {
            size_t seed = 0;

            boost::hash_combine(seed, p.id());
            boost::hash_combine(seed, p.name());

            return seed;
        }
    };
}

ostream& operator<<(ostream& out, const Person& p)
{
    out << "Person(id=" << p.id() << ", name=" << p.name() << ")";
    return out;
}


int main()
{
    unordered_set<int> uset1;

    uset1.max_load_factor(4.0);

    show_stat(uset1, "uset1");

    size_t prev_bucket_count = uset1.bucket_count();

    for(int i = 0; i < 100; ++i)
    {
        uset1.insert(rand() % 100);

        if (prev_bucket_count != uset1.bucket_count())
        {
            show_stat(uset1, "uset1 rehashed");
            prev_bucket_count = uset1.bucket_count();
        }
    }

    cout << "uset1 size: " << uset1.size() << endl;

    print(uset1, "uset1");

    auto where = uset1.find(21);

    if (where != uset1.end())
    {
        cout << "znalazlem element " << *where << endl;
    }

    show_stat(uset1, "uset1");

    uset1.rehash(128);

    show_stat(uset1, "uset1 after rehash");

    unordered_set<Person> people;

    people.insert(Person {1, "Kowalski"});
    people.emplace(4, "Nowak");
    people.insert(Person {2, "Anonim"});

    print(people, "people");

    cout << "Ilosc Kowalskich: " << people.count(Person {1, "Kowalski"}) << endl;
}

