#include <iostream>
#include <vector>
#include <deque>
#include <list>
#include <ctime>
#include <algorithm>
#include <cstring>

using namespace std;

/*
	Utwórz trzy sekwencje typu int: vector, deque i list. Wypelnij je wartosciami losowymi.
	Porównaj czasy tworzenia i wypelniania danymi sekwencji. Napisz szablon funkcji sortującej sekwencje
	vector i deque. Napisz specjalizowany szablon funkcji realizującej sortowanie dla kontenera list.
	Porównaj efektywność operacji sortowania.
*/

class ExpensiveObject
{
private:
	char* data_;
	size_t size_;
public:
	ExpensiveObject(const char* data) : data_(NULL), size_(std::strlen(data))
	{
		data_ = new char[size_+1];
		std::strcpy(data_, data);
	}

	ExpensiveObject(const ExpensiveObject& source) : data_(NULL), size_(source.size_)
	{
		data_ = new char[size_+1];
		std::strcpy(data_, source.data_);
	}

	~ExpensiveObject()
	{
		delete [] data_;
	}

	ExpensiveObject& operator=(const ExpensiveObject& source)
	{
		ExpensiveObject temp(source);
		swap(temp);

		return *this;
	}

	void swap(ExpensiveObject& other)
	{
		std::swap(data_, other.data_);
		std::swap(size_, other.size_);
	}

	const char* data() const
	{
		return data_;
	}

	size_t size() const
	{
		return size_;
	}

	bool operator<(const ExpensiveObject& other) const
	{
		if (std::strcmp(data_, other.data_) == -1)
			return true;
		return false;
	}
};

ExpensiveObject expensive_object_generator()
{
	static char txt[] = "abcdefghijklmn";
	static const size_t size = std::strlen(txt);

	std::random_shuffle(txt, txt + size);

	return ExpensiveObject(txt);
}

template <typename Container, typename Func>
void fill_container(Container& c, Func f, size_t n)
{
	for(size_t i = 0; i < n; ++i)
		c.push_back(f());
}

template <typename Container>
void sort_container(Container& c)
{
	sort(c.begin(), c.end());
}

template <typename T>
void sort_container(list<T>& l)
{
	l.sort();
}

clock_t get_clock()
{
	clock_t ct = clock();

	if (ct == clock_t(-1))
	{
		cerr << "Clock error" << endl;
		exit(1);
	}

	return ct;
}

//typedef vector<int> CollectionType;
//typedef deque<int> CollectionType;
//typedef list<int> CollectionType;

//typedef vector<ExpensiveObject> CollectionType;
//typedef deque<ExpensiveObject> CollectionType;
typedef list<ExpensiveObject> CollectionType;

int main()
{
    const int n = 1000000;

	clock_t t1, t2;

	CollectionType coll;

	cout << "Preparing data... Filling container." << endl;

	t1 = get_clock();

    //fill_container(coll, &rand, n);
    fill_container(coll, &expensive_object_generator, n);

	t2 = get_clock();

	cout << "Time: " << double(t2-t1)/CLOCKS_PER_SEC << "sec." << endl;

	cout << "\nStart sorting..." << endl;

	t1 = get_clock();

	sort_container(coll);

	t2 = get_clock();

	cout << "Time: " << double(t2-t1)/CLOCKS_PER_SEC << "sec." << endl;
}
