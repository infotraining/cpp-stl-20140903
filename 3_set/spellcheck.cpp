#include <iostream>
#include <fstream>
#include <set>
#include <string>
#include <chrono>
#include <unordered_set>
#include <iterator>
#include <algorithm>
#include <boost/algorithm/string.hpp>

using namespace std;

int main()
{
     // wczytaj zawartość pliku en.dict ("słownik języka angielskieog")
     // sprawdź poprawość pisowni następującego zdania:
    vector<string> words_list;
    string input_text = "this is an exple of snetence";
    boost::split(words_list, input_text, boost::is_any_of("\t "));

    ifstream fin("../en.dict");

    istream_iterator<string> start(fin);
    istream_iterator<string> end;

    unordered_set<string> dict(start, end);

    cout << "misspelled words: ";
//    for(auto& word : words_list)
//        if (!dict.count(word))
//            cout << word << " ";

    remove_copy_if(words_list.begin(), words_list.end(),
                   ostream_iterator<string>(cout, " "),
                   [&](const string& w) { return dict.count(w); });

    cout << endl;
}

