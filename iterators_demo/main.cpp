#include <iostream>
#include <algorithm>
#include <vector>
#include <list>
#include <deque>
#include <iterator>

using namespace std;

void print(int x)
{
    cout << x << " ";
}

template <typename InpIt, typename OutIt>
void mcopy(InpIt start, InpIt end, OutIt out)
{
    while(start != end)
    {
        *out++ = *start++;
    }
}

int main()
{
    vector<int> numbers = { 1, 2, 3, 4, 5, 6, 7, 8 };

    for_each(numbers.begin(), numbers.end(), &print);
    cout << "\n";


    list<int> lst_numbers(numbers.size());

    copy(numbers.begin(), numbers.end(), lst_numbers.begin());

    for_each(lst_numbers.begin(), lst_numbers.end(), &print);
    cout << "\n";

    auto lst_it = lst_numbers.begin();

    advance(lst_it, 3);

    cout << "*lst_it po advance: " << *lst_it << endl;

    cout << "distance: " << distance(lst_numbers.begin(), lst_it) << endl;

    vector<string> words;

    back_insert_iterator<vector<string>> bii(words);

    *bii = "One";
    *bii = "Two";
    *bii = "Three";

    for(const auto& w : words)
        cout  << w << " ";
    cout << endl;

    deque<string> inverted_words;

    copy(words.begin(), words.end(), front_inserter(inverted_words));

    for(const auto& w : inverted_words)
        cout  << w << " ";
    cout << endl;

    vector<int> evens;

    copy_if(numbers.begin(), numbers.end(), back_inserter(evens),
            [](int x) { return x % 2 == 0; });

    cout << "evens: ";
    for(auto x : evens)
        cout << x << " ";
    cout << "\n";

    copy(numbers.begin(), numbers.end(), inserter(evens, evens.begin() + 2));

    cout << "evens: ";
    for(auto x : evens)
        cout << x << " ";
    cout << "\n";

    cout << "Podaj dane:" << endl;
    istream_iterator<int> start(cin);
    istream_iterator<int> end;

    numbers.assign(start, end);

    cout << "\nWczytane dane: ";
    copy(numbers.begin(), numbers.end(), ostream_iterator<int>(cout, " "));
    cout << endl;
}

