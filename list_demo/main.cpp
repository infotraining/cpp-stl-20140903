#include <iostream>
#include <string>
#include <algorithm>
#include <list>
#include <vector>

using namespace std;

template <typename Container>
void print(const Container& container, const string& prefix = "container")
{
    cout << prefix << ": [ ";

    for(const auto& item : container)
        cout << item << " ";

    cout << "]" << endl;
}

int main()
{
    list<int> lst1 = { 1, 2, 3, 4, 5, 2, 3, 2, 3, 2 };

    print(lst1, "lst1");

    lst1.remove(2);

    print(lst1, "lst1");

//    auto garbage_starts = remove(lst1.begin(), lst1.end(), 2);

//    print(lst1, "lst1 po remove");

//    lst1.erase(garbage_starts, lst1.end());

    print(lst1, "lst1 po remove");

    list<int> lst2 = { 6, 3, 5, 6, 3, 3, 3, 3, 6, 9, 29 };

    lst1.sort();
    lst1.unique();

    print(lst1, "lst1 po sort i unique");

    lst2.sort();
    lst2.unique();

    print(lst2, "lst2 po sort i unique");

    lst1.merge(lst2);

    print(lst1, "lst1 po merge");
    print(lst2, "lst2 po merge");
}

