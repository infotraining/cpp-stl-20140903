#include <map>
#include <iostream>
#include <fstream>
#include <iterator>
#include <set>
#include <string>
#include <unordered_map>
#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;

/*
    Napisz program zliczający ilosc wystapien danego slowa
    w pliku tekstowym. Wyswietl wyniki zaczynajac od najczesciej wystepujacych slow.
*/

typedef vector<string> WordsContainer;

WordsContainer load_words_from_file(const string& file_name)
{
    ifstream file_book(file_name);

    if (!file_book)
    {
        cout << "Blad otwarcia pliku..." << endl;
        exit(1);
    }

    string line;
    WordsContainer words;

    while (getline(file_book, line))
    {
        boost::tokenizer<> tok(line);
        words.insert(words.end(), tok.begin(), tok.end());
    }

    return words;
}

class Iterator
{
    struct node { node* next;};
    node* node_;
public:
    Iterator& operator++() // ++it
    {
        node_ = node_->next;
        return *this;
    }

    Iterator operator++(int) // it++
    {
        Iterator temp(*this);
        node_ = node_->next;
        return temp;
    }
};

int main()
{
    WordsContainer words = load_words_from_file("../alice.txt");


    typedef unordered_map<string, int> Concordance;
    unordered_map<string, int> concordance(2048);

    // zliczenie ilosci wystapien slow
    for(auto it = words.cbegin();
        it != words.cend(); ++it)
    {
        concordance[boost::to_lower_copy(*it)]++;
    }

    multimap<int, string, greater<int>> concordance_by_freq;

    for(auto it = concordance.begin();
        it != concordance.end(); ++it)
    {
        concordance_by_freq.insert(make_pair(it->second, it->first));
    }

    int counter = 1;
    for(auto it = concordance_by_freq.begin();
        it != concordance_by_freq.end(); ++it, ++counter)
    {
        cout << it->second << " - " << it->first << endl;

        if (counter == 10)
            break;
    }

    cout << "\n\n";

    vector<Concordance::value_type> ten_most_freq(10);

    partial_sort_copy(concordance.begin(), concordance.end(),
                      ten_most_freq.begin(), ten_most_freq.end(),
                      [](const Concordance::value_type& p1, const Concordance::value_type& p2)
                        { return p1.second > p2.second; });

    for(auto& p : ten_most_freq)
        cout << p.first << " - " << p.second << endl;
}











