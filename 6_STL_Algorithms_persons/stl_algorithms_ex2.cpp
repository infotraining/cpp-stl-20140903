#include "person.hpp"
#include "utils.hpp"

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <functional>
#include <numeric>

using namespace std;

int main()
{
    vector<Person> employees;
    fill_person_container(employees);

    cout << "Wszyscy pracownicy:\n";
    copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));
    cout << endl;

    // wyświetl pracownikow z pensją powyżej 3000
    cout << "\nPracownicy z pensja powyżej 3000:\n";
    copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"),
            [](const Person& p) { return p.salary() > 3000.0; });

    // wyświetl pracowników o wieku poniżej 30 lat
    cout << "\nPracownicy o wieku poniżej 30 lat:\n";
    copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"),
            [](const Person& p) { return p.age() < 30; });

    // posortuj malejąco pracownikow wg nazwiska
    cout << "\nLista pracowników wg nazwiska (malejaco):\n";
    sort(employees.begin(), employees.end(),
         [](const Person& p1, const Person& p2) { return p1.name() > p2.name();});

    copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));

    // wyświetl kobiety
    cout << "\nKobiety:\n";
    copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"),
            [](const Person& p) { return p.gender() == Female; });


    // ilość osob zarabiajacych powyżej średniej
    cout << "\nIlosc osob zarabiajacych powyzej sredniej:\n";
    auto sum_of_salary = accumulate(employees.begin(), employees.end(),
                                    0.0, [](double val, const Person& p) { return val + p.salary(); });
    auto avg_salary = sum_of_salary / employees.size();


    cout << count_if(employees.begin(), employees.end(),
                     [=](const Person& p) { return p.salary() > avg_salary; }) << endl;

    cout << " avg salary: " << avg_salary << endl;
}
