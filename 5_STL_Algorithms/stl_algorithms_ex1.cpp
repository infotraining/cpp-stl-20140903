#include "utils.hpp"

#include <vector>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <cmath>
#include <numeric>

using namespace std;

class RandGen
{
    int range_;
public:
    RandGen(int range) : range_(range)
    {
    }

    int operator()() const
    {
        return rand() % range_;
    }
};

struct IsEven
{
    typedef int argument_type;
    typedef bool result_type;

    bool operator()(int x) const
    {
        return x % 2 == 0;
    }
};

int main()
{
    vector<int> vec(25);

    generate(vec.begin(), vec.end(), RandGen(30));

    print(vec, "vec: ");

    // 1a - wyświetl parzyste
    cout << "Even: ";
    copy_if(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "),
            //IsEven());
            [](int arg) { return arg % 2 == 0; });


    cout << endl;

    // 1b - wyswietl ile jest nieparzystych
    cout << "Count of odd numbers: "
         //<< count_if(vec.begin(), vec.end(), not1(IsEven())) << endl;
         << count_if(vec.begin(), vec.end(),
                     bind(modulus<int>(), placeholders::_1, 2)) << endl;

    // 1c - wyswietl ile jest parzystych
    cout << count_if(vec.begin(), vec.end(), IsEven()) << endl;

    // 2a - usuń liczby podzielne przez 3
    auto garbage_starts = remove_if(vec.begin(), vec.end(),
                                    [](int x) { return x % 3 == 0; });
    vec.erase(garbage_starts, vec.end());

    print(vec, "vec after remove div by 3: ");

    // 2b - usuniecie duplikatow
//    sort(vec.begin(), vec.end());
//    auto garbage_starts = unique(vec.begin(), vec.end());
//    vec.erase(garbage_starts, vec.end());

//    print(vec, "vec after unique: ");

    // 3 - tranformacja: podnieś liczby do kwadratu
    //transform(vec.begin(), vec.end(), vec.begin(), vec.begin(), multiplies<int>());

    transform(vec.begin(), vec.end(), vec.begin(), [](int x) { return x * x;});

//    double (*func)(double, double) = &pow;
//    transform(vec.begin(), vec.end(), vec.begin(),
//              bind(func, placeholders::_1, 2));


    // 4 - wypisz 5 najwiekszych liczb
    // TODO
    cout << "5 the gratest: ";
    nth_element(vec.begin(), vec.begin() + 5, vec.end(), greater<int>());
    copy(vec.begin(), vec.begin() + 5, ostream_iterator<int>(cout, " "));

    cout << endl;


    // 5 - policz wartosc srednia
    auto sum = accumulate(vec.begin(), vec.end(), 0.0);
    cout << "Avg: " << sum / vec.size() << endl;

    // 6 - wyswietl wszystkie mniejsze od 50
    copy_if(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "),
            [](int x) { return x < 50; });
    cout << endl;
}
