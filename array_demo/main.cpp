#include <iostream>
#include <array>
#include <tuple>

using namespace std;

template <typename Container>
void print(const Container& container, const string& prefix = "container")
{
    cout << prefix << ": [ ";

    for(const auto& item : container)
        cout << item << " ";

    cout << "]" << endl;
}

void legacy_code(int tab[], size_t size)
{
    cout << "legacy_code: ";
    for(int* it = tab; it != tab + size; ++it)
        cout << *it << " ";
    cout << "\n";
}

int main()
{
    int tab1[100] = {};

    array<int, 10> arr1 = { 1, 2, 3, 4, 5 };

    arr1[6] = -1;

    arr1.fill(0);

    cout << "size of arr1: " << arr1.size() << endl;

    legacy_code(arr1.data(), arr1.size());
}

