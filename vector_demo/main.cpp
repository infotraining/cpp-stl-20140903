#include <iostream>
#include <vector>
#include <string>
#include <stdexcept>
#include <algorithm>
#include <memory>

using namespace std;

template <typename T>
void show_stat(const vector<T>& vec, const string& text = "")
{
    cout << text << " - size: " << vec.size() <<
            ";  capacity - " << vec.capacity() << endl;
}

template <typename Container>
void print(const Container& container, const string& prefix = "container")
{
    cout << prefix << ": [ ";
//    for(auto it = begin(container);
//        it != end(container); ++it)
//    {
//        cout << *it << " ";
//    }

    for(const auto& item : container)
        cout << item << " ";

    cout << "]" << endl;
}

class Person
{
    int id_;
    string name_;

public:
    Person(int id, const string& name) : id_{id}, name_{name}
    {
        cout << "Person::ctor(" << id_ << ", " << name_ << ")" << endl;
    }

    Person(const Person& p) : id_{p.id_}, name_{p.name_}
    {
        cout << "Person::copy_ctor(" << id_ << ", " << name_ << ")" << endl;
    }

    Person& operator=(const Person& p)
    {
        cout << "Person::copy=(" << id_ << ", " << name_ << ")" << endl;

        if (this != &p)
        {
            id_ = p.id();
            name_ = p.name();
        }

        return *this;
    }

    Person(Person&& p) noexcept : id_{move(p.id_)}, name_{move(p.name_)}
    {
        cout << "Person::move_ctor(" << id_ << ", " << name_ << ")" << endl;
    }

    Person& operator=(Person&& p) noexcept
    {
        cout << "Person::move=(" << id_ << ", " << name_ << ")" << endl;

        if (this != &p)
        {
            id_ = move(p.id_);
            name_ = move(p.name_);
        }

        return *this;
    }

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }
};

ostream& operator<<(ostream& out, const Person& p)
{
    out << "Person(id=" << p.id() << ", name=" << p.name() << ")";
    return out;
}

vector<int> create_vec(int size)
{
    vector<int> vec;
    vec.reserve(size);

    for(int i = 0; i <  size; ++i)
        vec.push_back(i);

    return vec;
}

int main()
{
    vector<int> vec1;

    show_stat(vec1, "vec1");

    //vec1[0] = 1; // undefined behaviour

    vector<int> vec2 (10, -1);

    show_stat(vec2, "vec2");

    print(vec2, "vec2");

    vec2[5] = 6;

    print(vec2, "vec2");

    vector<int> vec3 = { 1, 2, 4, 5, 6, 6, 8, 19 };

    print(vec3, "vec3");

    vector<string> words1 { 10, "Hello" };

    print(words1, "words1");

    int tab1[10] = { 1, 2, 3, 5 };

    print(tab1, "tab1");

    vector<int> vec4(begin(tab1), end(tab1));

    print(vec4, "vec4");

    for(auto it = vec4.cbegin(); it != vec4.cend(); ++it)
        cout << *it << " ";
    cout << endl;

    try
    {
        vec4.at(12) = 10;
    }
    catch(const out_of_range& ex)
    {
        cout << ex.what() << endl;
    }

    // iteratory wsteczne
    for(auto rit = vec4.crbegin(); rit != vec4.crend(); ++rit)
    {
        cout << *rit << " ";
    }
    cout << endl;

    vec4[7] = 5;

    print(vec4, "vec4");

    auto where = find(vec4.rbegin(), vec4.rend(), 5);

    if (where != vec4.rend())
        cout << "Znalazlem: " << *where
             << " na pozycji: " << (where.base() - vec4.begin()) << endl;

    vector<int> vec5(vec4.rbegin(), vec4.rend());

    print(vec5, "vec5");

    // wstawianie elementow
    vector<int> numbers;

    //numbers.reserve(100);

    show_stat(numbers, "numbers");

    size_t prev_capacity = numbers.capacity();
    for(int i = 0; i < 1000; ++i)
    {
        numbers.push_back(i);
        if (numbers.capacity() != prev_capacity)
        {
            show_stat(numbers, "numbers");
            prev_capacity = numbers.capacity();
        }
    }

    numbers.resize(10);

    //numbers.shrink_to_fit();
    vector<int>(numbers).swap(numbers);

    show_stat(numbers, "numbers po clear");

    string str1 { "Hello" };
    string str2("World");

    vector<string> words2;

    words2.push_back(str1); // push_back(const string&)
    words2.push_back(string("Temp")); // push_back(string&&)
    words2.push_back(move(str2));
    words2.emplace_back("Text");

    print(words2, "words2");
    cout << "str1: " << str1 << endl;
    cout << "str2: " << str2 << endl;

    cout << "\n\nPeople:\n";

    vector<Person> people;

    Person p1 { 1, "Kowalski" };
    Person p2 { 2, "Nowak" };

    cout << "\nStarting push_back:" << endl;

    people.push_back(p1);
    people.push_back(move(p2));
    people.push_back(Person{3, "Adamska"});
    people.emplace_back(4, "Nijaki");


    print(people, "people");

    cout << "p1 - " << p1 << endl;
    cout << "p2 - " << p2 << endl;

    vector<unique_ptr<Person>> people_ptrs;

    unique_ptr<Person> uptr1(new Person { 7 , "Iks"});

    people_ptrs.push_back(unique_ptr<Person>(new Person(7, "Anonim")));
    people_ptrs.push_back(move(uptr1));
    people_ptrs.emplace_back(new Person(8, "Zet"));

    cout << "\npeople:\n";
    for(auto& ptr : people_ptrs)
        cout << *ptr << endl;

    vector<int> vec7 = create_vec(10);

    vec7.insert(vec7.begin() + 3, { -1, -2, -3 });

    print(vec7, "vec7");

    cout << "Max_size vector<Person>: " << people.max_size() << endl;
    cout << "Max_size vector<int>: " << vec7.max_size() << endl;

    vector<Person*> raw_ptrs_people;

    raw_ptrs_people.push_back(new Person {1, "Kowalski"});
    raw_ptrs_people.push_back(new Person {2, "Nowak"});

    for(auto ptr : raw_ptrs_people)
        delete ptr;

    raw_ptrs_people.clear();

}

