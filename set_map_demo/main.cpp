#include <iostream>
#include <set>
#include <map>
#include <tuple>
#include <functional>


using namespace std;

//namespace std
//{
template <typename T1, typename T2>
ostream& operator <<(ostream& out, const pair<T1, T2>& p)
{
    out << "(" << p.first << ", " << p.second << ")";

    return out;
}
//}

#include <iterator>

template <typename Container>
void print(const Container& container, const string& prefix = "container")
{
    cout << prefix << ": [ ";

    for(const auto& item : container)
        cout << item << " ";

    cout << "]" << endl;
}

class Person
{
    int id_;
    string name_;

public:
    Person(int id, const string& name) : id_{id}, name_{name}
    {
    }

    bool operator<(const Person& other) const
    {
        return id_ < other.id_;
    }

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }
};

class PersonCompByNameAsc
{
public:
    bool operator()(const Person& p1, const Person& p2) const
    {
        return p1.name() < p2.name();
    }
};

ostream& operator<<(ostream& out, const Person& p)
{
    out << "Person(id=" << p.id() << ", name=" << p.name() << ")";
    return out;
}



int main()
{
    set<int> set1 = {22, 12, 13 };

    set1.insert(5);
    set1.insert(2);
    set1.insert(8);
    set1.insert({7, 8, 9, 11});

    bool is_inserted;
    set<int>::iterator item_pos;

    tie(item_pos, is_inserted) = set1.insert(9);

    if (is_inserted)
    {
        cout << "Wstawiono " << *item_pos << endl;
    }
    else
        cout << *item_pos << " jest juz w zbiorze" << endl;

    auto item_7 = set1.find(7);

    if (item_7 != set1.end())
    {
        cout << "item: " << *item_7 << endl;
    }
    else
        cout << "Nie znalazlem elementu..." << endl;

    print(set1, "set1");

    auto it_range = set1.equal_range(9);

    cout << "lb(9): " << *it_range.first << endl;
    cout << "up(9): " << *it_range.second << endl;


    it_range = set1.equal_range(10);

    cout << "lb(10): " << *it_range.first << endl;
    cout << "up(10): " << *it_range.second << endl;

    if (it_range.first == it_range.second)
        set1.insert(it_range.first, 10);

    print(set1, "set1 po wstawieniu");

    set<int, greater<int>> set2(set1.begin(), set1.end());

    print(set2, "set2");

    auto person_by_name_desc =
            [](const Person& p1, const Person& p2) { return p1.name() > p2.name(); };

    /*
    class LambdaAnonymous
    {
    public:
        bool operator()(const Person& p1, const Person& p2)
        { return p1.name() > p2.name; };
    };
    */

    set<Person, PersonCompByNameAsc> people;

    Person p { 2, "Anonim" };

    people.insert(Person {1, "Kowalski"});
    people.insert(Person {4, "Nowak"});
    people.insert(move(p));

    print(people, "people");

    set<Person, function<bool (const Person& p1, const Person& p2)>> people_desc(person_by_name_desc);

    people_desc.insert(people.begin(), people.end());

    print(people_desc, "people_desc");

    typedef map<int, string> Map;
    Map map1 = { { 9, "Nine" }, { 8, "Eight" } };

    map1.insert(map<int, string>::value_type(1, "One"));
    map1.insert(pair<int, string>{ 2, "Two" });
    map1.insert(make_pair(0, "Zero"));

    print(map1, "map1");

    map1[10] = "Ten";

    cout << "10: " << map1.at(10) << endl;

    print(map1, "map1");

    copy(map1.begin(), map1.end(), ostream_iterator<Map::value_type>(cout, " "));
    cout << "\n";
}

