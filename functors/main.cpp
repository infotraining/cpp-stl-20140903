#include <iostream>
#include <algorithm>
#include <vector>
#include <list>
#include <memory>
#include <functional>

using namespace std;

template <typename Container>
void print(const Container& container, const string& prefix = "container")
{
    cout << prefix << ": [ ";

    for(const auto& item : container)
        cout << item << " ";

    cout << "]" << endl;
}

template <typename InIt, typename Function>
Function mfor_each(InIt start, InIt end, Function f)
{
    for(InIt it = start; it != end; ++it)
        f(*it);

    return f;
}

void print_item(int arg)
{
    cout << arg << ", ";
}

inline void square(int& x)
{
    x *= x;
}

struct Square
{
    void operator()(int &x)
    {
        x *= x;
    }
};

struct Sum
{
    int result = 0;

    void operator()(int x)
    {
        result += x;
    }
};

struct Lambda_35345
{
    string prefix;

    Lambda_35345(const string& prefix) : prefix(prefix) {}

    void operator()(int arg) { cout << prefix << arg << " "; }
};

bool is_leap_year(uint year)
{
    if ( 0 == year % 400 ) return true;
    if ( 0 == year % 100 ) return false;
    if ( 0 == year % 4 ) return true;
    return false;
}

struct IsLeap : public unary_function<uint, bool>
{
    //typedef uint argument_type;
    //typedef bool result_type;

    bool operator()(uint year) const
    {
        if ( 0 == year % 400 ) return true;
        if ( 0 == year % 100 ) return false;
        if ( 0 == year % 4 ) return true;
        return false;
    }
};

class Person
{
    int id_;
    string name_;
    uint age_;

public:
    Person(int id, const string& name, uint age) : id_{id}, name_{name}, age_{age}
    {
    }

    bool operator<(const Person& other) const
    {
        return id_ < other.id_;
    }

    bool operator==(const Person& other) const
    {
        if (id_ == other.id_)
            return name_ == other.name_;
        return false;
    }

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }

    void info() const
    {
        cout << "Osoba: " << id_ << " - " << name_ << " - age: " << age_ << endl;
    }

    bool is_older(uint threshold) const
    {
        return age_ > threshold;
    }

    void register_in_group(vector<const Person*>& group) const
    {
        group.push_back(this);
    }
};

int main()
{
    vector<int> vec = { 1, 2, 6, 7, 4, 5, 9, 10, 20, 34, 6, 9, 10 };

    mfor_each(vec.begin(), vec.end(), &print_item);
    cout << "\n";

    for_each(vec.begin(), vec.end(), Square());

    print(vec, "vec po square");

    Sum sum = mfor_each(vec.begin(), vec.end(), Sum());

    cout << "Sum of vec: " << sum.result << endl;

    string prefix = "item: ";
    int counter = 0;

    for_each(vec.begin(), vec.end(),
             [prefix, &counter](int arg) { cout << prefix << arg << " "; counter++; });

    cout << "\nCounter: " << counter << endl;

    int sum_result = 0;

    for_each(vec.begin(), vec.end(), [&sum_result](int x) { sum_result += x; });

    cout << "sum_result: " << sum_result << endl;

    list<int> years = { 1977, 1967, 1988, 2000, 2004, 2003 };

    auto leap_year = find_if(years.begin(), years.end(), IsLeap());

    if (leap_year != years.end())
        cout << "First leap year: " << *leap_year << endl;
    else
        cout << "No leap years" << endl;

    auto not_leap_year = find_if(years.begin(), years.end(), not1(ptr_fun(&is_leap_year)));

    if (not_leap_year != years.end())
        cout << "First not leap year: " << *not_leap_year << endl;
    else
        cout << "Only leap years" << endl;

    int threshold = 2000;

    auto first_21_century = find_if(years.begin(), years.end(),
                                    [=](int year) { return year > threshold; });

    if (first_21_century != years.end())
        cout << "first_21_century: " << *first_21_century << endl;

    vector<shared_ptr<Person>> people = { make_shared<Person>(1, "Kowalski", 66),
                                          make_shared<Person>(2, "Nowak", 34),
                                          make_shared<Person>(3, "Anonim", 45) };

    for_each(people.begin(), people.end(), mem_fn(&Person::info));

    cout << "Count > 40: " << count_if(people.begin(), people.end(),
                                       bind(&Person::is_older, placeholders::_1, 40)) << endl;

    vector<const Person*> friends;

    for_each(people.begin(), people.end(),
             //bind(&Person::register_in_group, placeholders::_1, ref(friends)));
             [&friends](shared_ptr<Person> p) { p->register_in_group(friends); });

    print(friends, "friends");

    vector<int> vec2;

    transform(vec.begin(), vec.end(), back_inserter(vec2),
              //bind(plus<int>(), placeholders::_1, 10));
              [](int x) { return x + 10; });

    print(vec, "vec");
    print(vec2, "vec2");

    cout << "Count >20: " << count_if(vec.begin(), vec.end(),
                                      //bind(greater<int>(), placeholders::_1, 20))
                                      [](int x) { return x > 20; })
         << endl;

    vector<int*> vec_ptrs;

    transform(vec.begin(), vec.end(), back_inserter(vec_ptrs), [](int& x) { return &x; });

    print(vec_ptrs, "vec_ptrs");
}

